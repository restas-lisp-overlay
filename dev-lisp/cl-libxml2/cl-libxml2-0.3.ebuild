inherit common-lisp-2 eutils

DESCRIPTION="High-level Common Lisp wrapper around libxml2 and libxslt libraries"
HOMEPAGE="http://code.google.com/p/cl-libxml2/"
SRC_URI="http://cl-libxml2.googlecode.com/files/${P}.tar.bz2"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~sparc ~x86"
IUSE=""

RDEPEND="dev-libs/libxml2
		 dev-libs/libxslt
		 >=dev-lisp/cffi-0.10.2
		 >=dev-lisp/garbage-pools-0.1.1
		 dev-lisp/puri
		 dev-lisp/flexi-streams
		 dev-lisp/metabang-bind
		 dev-lisp/lift"

CLSYSTEMS="${PN} ${PN}-test ${PN}-xslt-test ${PN}-xslt"

src_compile () {
	make -C foreign || die "Cannot build cllibxml2 helper library"
}

src_install () {
	common-lisp-install *.asd
	common-lisp-install tree/*.lisp
	common-lisp-install html/*.lisp
	common-lisp-install xpath/*.lisp
	common-lisp-install xslt/*.lisp
	common-lisp-install test/*.lisp
	common-lisp-symlink-asdf

	exeinto /usr/$(get_libdir)/ ; doexe foreign/cllibxml2.so

	dodoc ChangeLog
	docinto examples && dodoc examples/*
}